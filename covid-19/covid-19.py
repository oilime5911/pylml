# -*- coding: utf-8 -*-

import sys
import requests
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import argparse
from datetime import datetime, timedelta

from scipy.optimize import curve_fit, minimize_scalar

msg = "Dai dati pubblicati sull'epidemia COVID-19 in Italia analizza " + \
      "l'andamento dei decessi (o infetti, o guariti')."
parser = argparse.ArgumentParser(msg)
parser.add_argument("-b", "--before", type=int, default=0,
                    help="Limita l'analisi a -b giorni indietro.")
parser.add_argument("-l", "--load", action="store_true",
                    help="Ricarica i dati remoti.")

group = parser.add_mutually_exclusive_group()
group.add_argument("-d", "--deceased", action="store_true",
                   help="Usa i dati sui decessi (default).")
group.add_argument("-i", "--infected", action="store_true",
                   help="Usa i dati sugli infetti.")
group.add_argument("-c", "--cured", action="store_true",
                   help="Usa i dati sui guariti.")

args = parser.parse_args()
print(args)

column, colname = 'deceduti', 'decessi'
if args.infected:
    column, colname = 'totale_positivi', 'infetti'
elif args.cured:
    column, colname = 'dimessi_guariti', 'guariti'


def g_func(x, a, b, c):
    # funzione gassiana da parametrizzare
    return a * np.exp(-((b - x)**2)/c)


def get_doublerates(data):
    # Calcola i tempi di raddoppio in base ai dati
    # in forma di array.
    drs = []
    for i, d in enumerate(data[1:]):
        dr = -np.log(2)/(np.log(data[i-1]/data[i]))
        drs.append(dr)
    return np.array(drs)


datafile = 'data.csv'

# scarico i dati aggiornati
if args.load:
    url = 'https://raw.githubusercontent.com/pcm-dpc/' \
          'COVID-19/master/dati-andamento-nazionale/' \
          'dpc-covid19-ita-andamento-nazionale.csv'
    r = None
    try:
        r = requests.get(url)
    except Exception:  # ConnectionError:
        pass
    if r:
        with open(datafile, 'wb') as f:
            f.write(r.content)
    else:
        print(f'Url {url} non raggiungibile.\n'
              'Uso i dati della sessione precedente.')

# carico i dati tramite pandas
data = pd.read_csv(datafile)

if args.before > 0:
    last = len(data) - args.before - 1
    data = data.truncate(after=last)

# valori numerici per le ascisse
xvalues = [i for i in range(len(data))]

# etichette per le ascisse: dd/mm
xlabels = [f'{d[8:10]}/{d[5:7]}' for d in data['data']]

# valori per le ordinate,
# dai totali giornalieri si deduce l'incremento giornaliero
dd = data[column].tolist()
yvalues = []
for i, y in enumerate(dd):
    if i == 0:
        yvalues.append(y)
    else:
        yvalues.append(dd[i]-dd[i-1])

# ottimizzazione della funzione gaussiana
try:
    g_popt, g_pcov = curve_fit(g_func, xvalues, yvalues)
except RuntimeError:
    print(f"Quantità di dati insufficiente: {len(data)} righe.")
    sys.exit(0)

# grafico dell'andamento dei dati
plt.plot(xvalues, yvalues, 'b-', color='tab:red', label='dati')

# andamento stimato, esteso sui prossimi 7 giorni
xvalues.extend(range(len(data), len(data)+7))
lastday = data['data'].tolist()[-1]
oneday = timedelta(days=1)
lastday = datetime.strptime(lastday, '%Y-%m-%dT%H:%M:%S')

nextd = lastday + oneday
for i in range(7):
    xlabels.append(datetime.strftime(nextd, '%d/%m'))
    nextd += oneday

xx = np.array(xvalues)
plt.plot(xx, g_func(xx, *g_popt), 'r-', linestyle='dotted',
         label='andamento gaussiano', color='tab:orange')

plt.xticks(xx, xlabels, rotation=90)

print(f'\nDati aggiornati al {lastday}\n')

# Tempi di raddoppio
double_rates = get_doublerates(g_func(xx, *g_popt))
print('Tempi di raddoppio in giorni')
print(f'  medio: {double_rates.mean():0.4}')
print(f'  ultimi 7 gg')
result = []
for d in range(7):
    dr = double_rates[-d-7]
    result.append([xlabels[-d-7], dr])
maxv = max([v for l, v in result])
scale = 80/maxv
print('\n'.join([f' {l}\t{v:0.4}' for l, v in result]))

# Calcolo del picco
r = minimize_scalar(lambda x: -g_func(x, *g_popt), bounds=[1, len(data)*2])
xpeak = int(np.round(r['x']))
try:
    daypeak = xlabels[xpeak]
except IndexError:
    # la data di picco è esterna alle etichette previste
    dp = lastday + oneday*(xpeak - len(xlabels) + 7)
    daypeak = datetime.strftime(dp, '%d/%m')
print(f"\nPrevisione picco: {daypeak}, {int(-r['fun'])} nuovi {colname}")
peak = g_func(xpeak, *g_popt)
plt.plot([xpeak], [peak], 'bo')

# Proiezioni
# Conteggi da modello gaussiano
print(f'\nTotale {colname}:')
print(f'\tprevisti\treali\tincremento giornaliero')
Y = g_func(xvalues, *g_popt)
modelcounts = 0
flagpeak = ''
for i, y in enumerate(Y):
    modelcounts += y
    flagpeak = ''
    if i == xpeak:
        flagpeak = "🞀"
    if i >= len(data)-7:
        try:
            realcounts = f'\t{dd[i]:5}'
        except IndexError:
            realcounts = "\t"
        print(f'  {xlabels[i]}\t{int(np.round(modelcounts)):8}' +
              f' {realcounts}\t{int(Y[i]):4}{flagpeak}')

# Ricerca del valore x con g_func(x) < 10
ystop = 10
x = len(Y)
xday = datetime.strptime(f'2020/{xlabels[-1]}', '%Y/%d/%m')
while y > ystop:
    x += 1
    modelcounts += y
    y = g_func(x, *g_popt)
    xday += oneday
# print(x, xday, int(np.round(y)), int(np.round(modelcounts)))
dstop = datetime.strftime(xday, "%d/%m")
print(f'\nIncremento < {ystop} casi previsto il {dstop} ' +
      f'con un totale di {int(np.round(modelcounts))} {colname}.')

# Visualizzazione grafico
plt.ylabel(f'incremento {colname} giornaliero')
plt.xticks(np.arange(xvalues[0], xvalues[-1]+1, 1))
plt.grid(b=True, which='major', color='#222222', linestyle='-')
plt.title(f'\nDati aggiornati al {lastday}')
plt.legend()
plt.show()
