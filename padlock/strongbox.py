""" Una cassaforte ha una chiusura a combinazione di N cifre.
Si forniscono una serie di combinazioni di prova accompagnate da
informazioni sul numero di cifre presenti nella combinazione esatta
e il numero di cifre eventualmente in posizione corretta.
Es.
(1.0),'614' indica che una sola cifra è presente, ma nessuna è in corretta.

Strategia:
si genera un l'insieme delle soluzioni (candidates) come matrice di stringhe
di N colonne x 10 righe (se le cifre sono numeriche).
Si ordinano le combinazioni di prova in base alle informazioni, per ciascuna
si escludono alcuni elmenti da candidates e si popola escluded.
Il dizionario resolve ha come chiave la cifra e la posizione, se una colonna
di candidates ha un solo elemento quello appartiene alla soluzione e si porta
in resolve.

Quando resolve ha N elementi rappresenta la soluzione definitiva.
"""

import numpy as np


class StrongBox(object):
    # Si assume che la combinazione sia composta di cifre numeriche
    digits = range(0, 10)

    def __init__(self, data):
        data.sort()
        self.data = data
        self.N = len(data[0][1])
        self.candidates = np.array([self.digits] * self.N).T
        self.excluded = {}
        self.resolve = {}
        print(self.data)

    def solve(self):
        data = self.data
        resolve = {}
        while len(self.resolve) < self.N:
            for d in data:
                self._attemp(d)
                if len(self.resolve) == self.N:
                    break
                else:
                    print(self.format_resolve())

            if self.resolve == resolve:
                return "SENZA SOLUZIONE"

            resolve == self.resolve

        return self.format_resolve()

    # def _analyze(self):
    #     # Se una colonna contiene una sola cifra questa è corretta.
    #     # => si aggiunge al risultato
    #     cdts = self.candidates
    #     for col in range(self.N):
    #         _ = [n for n in cdts.T[col] if n != -1]
    #         print(_)

    def is_excluded(self, n):
        e = self.excluded.setdefault(n, [])
        if len(e) == self.N:
            return True
        return False

    def is_unique(self, n):
        # se n è unica in candidates significa che è corretta
        _ = [x for x in self.candidates[n] if x == n]
        return len(_) == 1

    def remove_others(self, col, n):
        # rimuove dalla colonna col le cifre diverse da n
        for c in range(10):
            if c != n:
                self.candidates[c][col] = -1
                self.excluded.setdefault(c, [])
                if col not in self.excluded[c]:
                    self.excluded[c].append(col)

    def format_resolve(self):
        if len(self.resolve) == self.N:
            res = [(v, k) for k, v in self.resolve.items()]
            res.sort()
            return ', '.join([str(k) for v, k in res])
        else:
            res = ['?'] * self.N
            for k, v in self.resolve.items():
                res[v] = str(k)
            return ', '.join(res)

    def _attemp(self, rule):
        # Applica la regola rule.
        # rule: (exsists, correct), (ciphers)
        # exists: numero di cifre presenti
        # correct: numero di cifre corrette
        cdts = self.candidates
        excl = self.excluded
        (exists, correct), attempt = rule
        print("\nPROVA", ''.join([str(a) for a in attempt]),
              f"cifre presenti: {exists}, cifre corrette: {correct}")
        if exists == 0:
            # nessuna cifra presente:
            # => elimina le cifre da candidates
            for a in attempt:
                _c = [-1] * self.N
                cdts[a] = _c
                if a not in excl:
                    excl[a] = [*range(self.N)]

        elif correct == 0:
            # almeno una cifra presente, nessuna corretta:
            # => elimina ciascuna cifra dalla colonna corrisponente
            for col, a in enumerate(attempt):
                cdts[a][col] = -1
                excl.setdefault(a, [])
                if col not in excl[a]:
                    excl[a].append(col)

            exists = [a for a in attempt if not self.is_excluded(a)]
            for col, a in enumerate(attempt):
                if a in exists and a not in self.resolve:
                    if self.is_unique(a):
                        c = list(cdts[a]).index(a)
                        self.resolve[a] = c
                        self.remove_others(c, a)

        else:
            # alcune cifre corrette:
            # => si cercano le cifre nelle colonne di candidates
            # => se esiste significa che è corretta
            # => quindi si eliminano tutte le altre cifre dalle altre colonne
            # Es. (1,1), 682
            # Eliminare 6 dalle 2° e 3°, 8 dalle 1° e 2°, 2 dalle 1° e 2°
            for col, a in enumerate(attempt):
                for c in range(self.N):
                    if c != col:
                        cdts[a][c] = -1
                        excl.setdefault(a, [])
                        # if col not in excl[a]:
                        if c not in excl[a]:
                            excl[a].append(c)

            # se una cifra esiste in una sola colonna è corretta:
            # => escludere le altre cifre dalla colonna
            for col, a in enumerate(attempt):
                if self.is_unique(a):
                    self.resolve[a] = col
                    self.remove_others(col, a)

        # self._analyze()


# ---------------------------------------
if __name__ == '__main__':
    data = [[(1, 1), (6, 8, 2)],
            [(1, 0), (6, 1, 4)],
            [(2, 0), (2, 0, 6)],
            [(0, 0), (7, 3, 8)],
            [(1, 0), (3, 8, 0)]]

    sb = StrongBox(data)
    print(sb.solve())
